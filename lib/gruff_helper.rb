# frozen_string_literal: true

# Helper methods for Gruff charts
module GruffHelper # rubocop:disable Metrics/ModuleLength
  # round value up to display an horizontal bar at the top of the chart
  def self.round_up(value)
    value = value.to_f
    save_value = value
    decimals = 0
    while value >= 1
      value /= 10
      decimals += 1
    end

    pow = if decimals <= 1
            1
          elsif [2, 3].include? decimals
            10
          elsif decimals == 4
            100
          else
            10**(decimals - 1)
          end

    value = (save_value / pow).ceil * pow
    [value, 1].max
  end

  # Draws horizontal background lines and labels
  def gruff_default_chart(data, filename, size, **options) # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
    g = Gruff::Line.new(size)
    # g.show_vertical_markers = true
    configure_default_chart(g)
    g.title = nil
    g.hide_legend = data.length <= 1
    g.hide_dots = true
    g.labels = gruff_labels(data[data.keys.first], 24.hours, '%a %d')
    g.marker_font_size = 25
    data.each do |name, values|
      g.data(name, values.values)
    end
    g.minimum_value = options[:minimum_value] || 0
    g.minimum_x_value = options[:minimum_x_value] || 0
    g.maximum_value = options[:maximum_value] || g.maximum_value
    g.baseline_value = options[:baseline_value] || g.baseline_value
    colors = options[:colors] || ['#CB0101', '#E69005', '#A5C522', '#119437']
    g.theme = {
      colors: colors,
      marker_color: '#DDDDDD',
      font_color: '#000000',
      background_colors: 'transparent'
    }
    g.x_axis_label = options[:x_axis_label]
    g.y_axis_label = options[:y_axis_label]
    g.custom_markers = options[:custom_markers]
    g.line_width = options[:line_width] || g.line_width
    write_daily_horizontal_lines(g, data)
    g.write(filename)
  end

  def write_daily_horizontal_lines(gruff, hash)
    set = 0
    hash.each do |_name, data|
      break unless set.zero?

      set = 1
      index = 0
      data.each do |key, _|
        # if DateTime.parse(key).hour.zero?
        #   gruff.reference_lines[index] = { index: index, color: 'grey' }
        # end
        gruff.reference_lines[index] = { index: index, color: 'grey' } if DateTime.parse(key).day.even?
        index += 1
      end
    end
  end

  def gruff_bar_chart(data, filename, size, **options) # rubocop:disable Metrics/AbcSize
    g = Gruff::Bar.new(size)
    configure_default_chart(g)
    g.title = nil
    g.hide_legend = true
    g.marker_font_size = 25
    g.labels = gruff_labels(data[data.keys.first], 24.hours, '%a %d')
    data.each do |name, values|
      g.data(name, values.values)
    end
    g.minimum_value = 0
    g.maximum_value = 5
    g.theme = {
      colors: ['#119437'],
      marker_color: '#DDDDDD',
      font_color: '#000000',
      background_colors: 'transparent'
    }
    g.x_axis_label = options[:x_axis_label] || ''
    g.y_axis_label = options[:y_axis_label] || ''
    g.write(filename)
  end

  # data format : { name: value, name2: value2 }
  def gruff_pie_chart(data, filename, size, **options)
    g = Gruff::Pie.new(size)
    g.hide_legend = true
    configure_default_chart(g)
    g.title = options[:title]
    g.hide_labels_less_than = 9999
    data.each do |name, value|
      g.data(name, value)
    end
    g.theme = {
      colors: GSHEETS_COLORS,
      marker_color: '#DDDDDD',
      font_color: '#000000',
      background_colors: 'transparent'
    }
    g.write(filename)
  end

  def gruff_labels(data, step, format = '%d %m %Y')
    data.to_a.map.with_index do |datum, index|
      datum_time = Time.zone.parse(datum[0])
      next unless step_data?(data, step, datum_time)

      [index, I18n.localize(datum_time, format: format)]
    end.compact.to_h
  end

  def gruff_data_average(data)
    relevant_values = data.compact.values
    return 0 if relevant_values.length.zero?

    relevant_values.sum / relevant_values.length
  end

  def gruff_data_ratio(data1, data2)
    data1.map do |t, v|
      if v.nil? || data2[t].nil? || data2[t].zero?
        [t, nil]
      else
        [t, v / data2[t]]
      end
    end.to_h
  end

  # Compute stable frequency of measured/simulated data
  # This will force empty data when data is missing
  # graphes need this to have a linear time scale
  def gruff_compute_stable_average_week(data) # rubocop:disable Metrics/AbcSize
    ret = {}

    (0..6).each do |day|
      break if data.empty?

      (0..23).each do |hour|
        start_t = @start_date + hour.hours + day.days
        end_t = start_t + 1.hour

        # We assume data keys are sorted
        values = data.take_while do |time, _|
          time = DateTime.parse(time)
          time >= start_t && time < end_t
        end

        data = data.drop(values.length)

        # Get values only
        values = values.map { |_, v| v }.compact

        # Average
        ret[start_t.to_s] = values.empty? ? nil : values.sum / values.length
      end
    end

    ret
  end

  # Prawn doesnt support the i18n format we have : iO₃
  LABELS = {
    co2_index: 'iCO<sub>2</sub>',
    pm25_index: 'iPM<sub>2.5</sub>',
    pm10_index: 'iPM<sub>10</sub>',
    no2_index: 'iNO<sub>2</sub>',
    o3_index: 'iO<sub>3</sub>',
    tvoc_index: 'iCOV<sub>L</sub>'
  }.freeze

  GSHEETS_COLORS = %w[#4285F4 #FBBC06 #EA4335 #34A853 #FB6D02 #46BDC6].freeze

  def custom_legend(pdf, pos, labels, **options) # rubocop:disable Metrics/AbcSize
    x, y = pos
    cell_height = options[:height] / labels.length
    margin = 5
    width = options[:width]

    pdf.font_size 8 do
      pdf.stroke do
        labels.each_with_index do |label, i|
          pos = [x, y - i * (cell_height + margin)]
          pdf.fill_color(GSHEETS_COLORS[i][1..-1])
          pdf.fill_rectangle(pos, width, cell_height)
          pos = [pos[0] + margin + width, pos[1] - cell_height / 2 + margin]
          pdf.fill_color('000000')
          pdf.text_box LABELS[label], at: pos, inline_format: true
        end
      end
    end
  end

  private

  # Change default no data message
  # https://github.com/topfunky/gruff/blob/master/lib/gruff/base.rb#L230
  def configure_default_chart(gruff)
    gruff.no_data_message = 'Pas de données'
  end

  # Center labels around midday
  def step_data?(data, step, datum_time)
    ((datum_time - Time.zone.parse(data.first[0]) - 12.hours) % step).zero?
  end
end
