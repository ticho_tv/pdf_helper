# frozen_string_literal: true

# @todo Document please
module PdfBuilder # rubocop:disable Metrics/ModuleLength
  def h1(pdf, title)
    pdf.font_size(20) do
      pdf.text title, inline_format: true
    end
  end

  def h2(pdf, title)
    pdf.font_size(12) do
      pdf.text title, inline_format: true
    end
  end

  def intro(pdf, text)
    pdf.font_size(15) do
      pdf.text text, inline_format: true
    end
  end

  def block(pdf, title, pos, **options)
    options[:pdf] = pdf
    padded_box(pos, 10, options) do
      pdf.text(title, inline_format: true) unless title.empty?
      yield if block_given?
    end
  end

  def bullet_list(pdf, entries)
    margin_width = 20
    bullet = '-'

    entries.each do |entry|
      y = pdf.cursor
      pdf.text_box(bullet, at: [0, y], width: margin_width, align: :center)
      # Using experimental API to get the rendered height
      formatted_entry = Prawn::Text::Formatted::Parser.format(entry)
      box = Prawn::Text::Formatted::Box.new(formatted_entry, document: pdf, at: [margin_width, y])
      box.render
      pdf.move_down(box.height)
    end
  end

  # TODO : Delete ?
  # Unused after removal of chart comments
  def caption(pdf, text)
    # Italic
    pdf.text('<i>%<text>s</i>' % { text: text }, inline_format: true)
  end

  def measure_text_height(pdf, text)
    # Measure using experimental API
    box = Prawn::Text::Box.new(text, document: pdf)
    box.render(dry_run: true)
    box.height
  end

  def line_height(pdf)
    char_h = measure_text_height(pdf, '0')
    # Trying to measure the line spacing
    two_chars_h = measure_text_height(pdf, "0\n0")
    two_chars_h - char_h
  end

  def arrow(pdf, pos, size) # rubocop:disable Metrics/AbcSize
    x, y = pos
    rect_size = size / 2
    triangle_part_height = rect_size / 2

    x1 = x + rect_size
    x2 = x + rect_size * 2
    y1 = y + triangle_part_height
    y2 = y - triangle_part_height * 3
    y3 = y - triangle_part_height

    pdf.fill_polygon([x1, y1], [x1, y2], [x2, y3])
    pdf.fill_rectangle(pos, rect_size + 1, rect_size)
  end

  #
  # Pads a bounding box by nesting another bounding_box inside it
  # Can also take bg_color as a parameter (you have to specify height for this to work)
  # Syntax:
  # padded_box([x,y], <padding:Integer>,pdf: pdf, <opts...>, :bg_color => <hex:String>) do
  #   <your block content here>
  # end
  # Note : since the modification to make it usable with explicit blocks,
  # the pdf (Prawn document) needs to be passed in the arguments
  #
  def padded_box(*args, &block) # rubocop:disable Metrics/AbcSize
    opt = args.extract_options!
    padding = args.delete_at(1)
    pdf = opt[:pdf]
    pdf.bounding_box(*args, opt) do
      save = pdf.stroke_color
      pdf.stroke_color 'D3D3D3'
      pdf.stroke_bounds
      pdf.stroke_color save
      pdf.bounding_box(
        [padding, pdf.bounds.height - padding],
        width: pdf.bounds.width - 2 * padding,
        height: pdf.bounds.height - 2 * padding
      ) do
        block.call
      end
    end
  end

  def calc_cell_size(size, count, **options)
    gutters_size = (count - 1) * options[:gutter]
    (size - gutters_size) / count
  end

  # Create a simple grid and compute :cell_width
  # and :cell_height. Can be used in conjunction with cell_pos method.
  def grid_create_helper(pos, **options)
    cell_width = calc_cell_size(options[:width], options[:columns], options)
    cell_height = calc_cell_size(options[:height], options[:rows], options)

    options.merge!(
      pos: pos,
      cell_width: cell_width,
      cell_height: cell_height
    )
  end

  # Return the position of a cell inside the grid
  # defined by grid_create_helper
  def cell_pos(grid, row, column)
    x, y = grid[:pos]
    x += column * (grid[:cell_width] + grid[:gutter])
    y -= row * (grid[:cell_height] + grid[:gutter])
    [x, y]
  end

  def cell_width(grid, column_count)
    w = column_count * grid[:cell_width]
    w += (column_count - 1) * grid[:gutter] if column_count > 1
    w
  end

  # TODO: check if we need it in supervision aswell
  # @return Hash { value => color }
  def custom_markers(markers, values)
    max = if values.max > markers.keys.max
            values.max
          else
            markers.keys.select { |v| v >= values.max }.min
          end
    min = markers.keys.min
    spread = max - min
    intermediate_values = [spread / 4.0, spread / 2.0, (3.0 * spread / 4.0)].map { |v| v + min }
    # Prevent override by grey lines
    markers.each { |k, _v| intermediate_values.delete(k) }
    markers.merge intermediate_values.map { |v| [v, '#D3D3D3'] }.to_h
  end

  def create_chart_image(name, data, size, chart_method, **options)
    FileUtils.mkdir_p(Rails.root.join('tmp', 'charts'))
    file = chart_method.call(data, Rails.root.join('tmp', 'charts', name + '.png'), size, options)
    file
  end

  # TODO: Replace default -> line
  def create_default_chart_image(name, data, size, **options)
    create_chart_image(name, data, size, method(:gruff_default_chart), **options)
  end

  def create_bar_chart_image(name, data, size, **options)
    create_chart_image(name, data, size, method(:gruff_bar_chart), **options)
  end

  def create_pie_chart_image(name, data, size, **options)
    create_chart_image(name, data, size, method(:gruff_pie_chart), **options)
  end

  # TODO: Refactor !!
  # val and height cannot be options if the code breaks when they are absent
  def display_indicator_graph(pdf, pos, **options)
    x, y = pos
    old_color = pdf.fill_color
    old_cursor = pdf.cursor
    colors = %w[CB0101 E69005 A5C522 119437 007BFF 0069D9]
    val = colors.length - options[:val] - 1
    cell_height = options[:height] / colors.length

    pdf.stroke do
      colors.each_with_index do |color, i|
        pos = [x, y - i * cell_height]
        pdf.fill_color(color)
        pdf.fill_rectangle(pos, options[:width], cell_height)
        pdf.fill_color('000000')
        pdf.font Prawn::Font::AFM::BUILT_INS[1] do
          pdf.text_box((colors.length - i - 1).to_s, at: pos, width: options[:width], height: cell_height,
                                                     align: :center, valign: :center)
        end
      end
    end

    if !val.negative? && val < colors.length
      pdf.fill_color(colors[val])
      pos = [x - cell_height - 5, y - val * cell_height - cell_height / 4]
      arrow(pdf, pos, cell_height)
    end

    pdf.fill_color(old_color)
    pdf.move_cursor_to(old_cursor)
  end

  # Compute the max value that should be taken for the chart
  # We either take the max value, or the min marker that is above the max value
  # TODO : the top line draw should be calculated here
  def maximum_value(data_set, markers)
    return nil unless (max = data_set.flatten.max)

    max_marker = markers.sort_by { |k, _v| k }.find { |k, _v| k >= max }
    max_marker ? max_marker[0] : max
  end

end
